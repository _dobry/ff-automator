from selenium import webdriver
from selenium.webdriver.remote import webelement
import time


driver = webdriver.Firefox()

# Go to page URL
driver.get("http://www.fabryka-formy.pl/kluby/21/poznan-baltyk/")

# Find calendar
calendar = driver.find_element_by_id("calendar")
calendar_rows = calendar.find_elements_by_tag_name("tr")

# Loop on the calendar rows
calendar_dict = {}
days = []


class TrainingClass:
    hour_start = time.struct_time
    hour_end = time.struct_time
    workout_name = ""
    trainer = ""
    place = ""
    difficulty = ""
    reservation_button = "webelement"

    def __repr__(self):
        return "{0} starts at {1:02}:{2:02} and ends at {3:02}:{4:02}\n".\
            format(self.workout_name, self.hour_start.tm_hour,
                   self.hour_start.tm_min,  self.hour_end.tm_hour, self.hour_end.tm_min)

    def make_reservation(self):
        self.reservation_button.click()
        return True

for row_index, row in enumerate(calendar_rows):
    row_columns = row.find_elements_by_tag_name("td" if row_index != 0 else "th")

    for column_index, column in enumerate(row_columns):
        if row_index == 0:
            if column_index > 0:
                days.append(row_columns[column_index].text)
                calendar_dict[days[-1]] = []
        if row_index > 0:
            if column_index > 0:
                trainings = column.find_elements_by_tag_name("div")
                trainings_list = []
                for training in trainings:
                    trainingObj = TrainingClass()
                    trainingObj.difficulty = training.find_element_by_class_name("difficulty").text
                    trainingObj.trainer = training.find_element_by_class_name("trainer").text
                    trainingObj.place = training.find_element_by_class_name("place").text
                    trainingObj.workout_name = training.find_element_by_class_name("workout").text
                    hours_str = training.find_element_by_class_name("hours").text
                    start, end = hours_str.split("-")
                    trainingObj.hour_start = time.strptime(start.strip(), "%H:%M")
                    trainingObj.hour_end = time.strptime(end.strip(), "%H:%M")
                    trainingObj.reservation_button = training.find_element_by_tag_name("button")
                    trainings_list.append(trainingObj)

                calendar_dict[days[column_index - 1]] += trainings_list

calendar_dict["środa"][6].make_reservation()

reservation_modal = driver.find_element_by_id("bookWorkout").find_element_by_tag_name("div")

attr = reservation_modal.get_attribute("ng-if")

print(reservation_modal.text)

if attr.find("form-hurry-up") != -1:
    reservation_modal.find_element_by_id("name").send_keys("Justyna")
    reservation_modal.find_element_by_id("phone").send_keys("783689428")
    reservation_modal.find_element_by_tag_name("button").click()
    print(driver.find_element_by_id("thankYou").text)
    print(reservation_modal.text)

driver.close()

 # print("row")

# Find all buttons on the page
# all_buttons = calendar.find_elements_by_class_name("btn")


